# Jonas notes

Publish my jupter notebooks as a jupter-book on gitlab pages.

## How to patch

```sh
# Edit stuff (e.g. _jbook.p/_includes/topbar.html)

# Create patchfile
diff -ruN _jbook _jbook.p > patches.patch

# To be applied as follows
patch -p0 < patches.patch

```
