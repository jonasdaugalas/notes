// Put your custom javascript here

// Start with Sidebar closed
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('js-textbook').classList.remove('js-show-sidebar');
    document.getElementById('js-sidebar-toggle').classList.remove('is-active');
});
