import sys
import os
import yaml


def main():
    toc_path = sys.argv[1]
    with open(toc_path, 'r') as f:
        toc = yaml.safe_load(f)

    for entry in toc:
        if 'url' in entry and not entry['url'].startswith('/'):
            entry['url'] = '/' + entry['url']

    with open(toc_path, 'w') as f:
        yaml.dump(toc, f)


if __name__ == '__main__':
    main()
