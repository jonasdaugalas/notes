# MSc Informatics TUM application essay

2019

Essay topic 2: The influence of social networks on human society.


## Is it still us who know ourselves best?


### Introduction

The emergence and massive adoption of social networking services (SNS) are having a multidimensional influence on human society. From taking the role of media for breaking news [1], to becoming one of the most important tools in recruitment process [2][3][4]. From becoming our go-to resource for video consumption (according to the Alexa page ratings of 2019, YouTube is the 2nd most visited site in the world [5]), to enabling our messages to potentially reach millions of people within seconds. From boosting internet addiction [6][7], to contributing to the issues of cyberbullying [8][9]. In this essay I will discuss one specific influence of the SNSs on human society, focusing on the amount and value of data possessed by the SNSs. I will argue that SNSs are starting to know us better than we know ourselves.


### What data are collected?

Most of the users could probably quickly list some obvious pieces of information being collected by SNSs. Such items include personal profile descriptions; friend/connection lists; posted texts, images, and videos; likes; shares; group memberships; location check-ins; private communications in the form of messages, audio, and video calls. These items are the main content created and consumed by the users.

The next list of information accessible to the SNSs still includes the information we willingly share, only that it is not at the center of our attention. Some items from this list would be: personal settings (such as privacy, notifications and language preferences); games played on the platform; log-in, log-out times; external applications for which one chooses to authenticate with their SNS profile.

Finally, the third set of collected information may not be as obvious to people without the knowledge of specific platform technology. This list includes: metadata of the client used to access the service e.g. browser and operating system versions; visits of external webpages which have integration with SNSs e.g. "share"/"like" buttons, or commenting sections; feed consumption and video playback patterns e.g. fast scrolling, stopping at particular content, rewinding, pausing etc.; reaction to autoplay content; text typing patterns (quick, heavy editing, copy-paste); device IP address, mouse pointer movements; user's connections with people outside the network (e.g. by allowing access to external contacts such as email and phone book).


### Information that we did not share

Having the described data collected over time even for a single user can already tell us a great deal about the views, interests, culture, and habits of the person. However, in the case of SNSs, which are collecting such data for millions of users, there can be far more interesting things discovered.

To start with, there is some information that we intentionally or unintentionally have not provided, but which is not necessarily unknown to the platform or other users. Studies show how various private attributes can be inferred by analyzing social networks: from inferring gender with over 90% accuracy [10], or uncovering one's school/college, matriculation year and department with over 80% accuracy while having as few as 20% of users with known attributes [11]; to figuring out social roles and statuses of various industries actors [12].


### Information that no one shares

Taking one step further, there is another category of private information to be inferred about us - information which not only is not shared by us but also is not common to be shared at all. A model was presented being able to "correctly discriminate between homosexual and heterosexual men in 88% of cases, African Americans and Caucasian Americans in 95% of cases, and between Democrat and Republican in 85% of cases" [13]. Another study presented a method for inferring scores of personality traits in a taxonomy known as "Big Five" or "five-factor model" [14] within just over 10% resolution compared to the state of the art tests [15]. What is more, there are successful attempts in gaining insights as deep as psychiatric disorders: one example being a study proposing a machine learning framework to infer mood instability with 96% accuracy [16] from small samples gathered through active sensing combined with large-scale data from social media.

The ambitions in social network data analysis efforts do not stop at information inference. Another goal of researchers is future prediction. There are numerous studies investigating future prediction from social network data (predicting new social links in the network [17][18]; predicting adoption of a product, service, or opinion [19]; predicting speed, scale and range of information reach [20]). One interesting example is a study demonstrating that "social media contains useful signals for characterizing the onset of depression in individuals" [21]. The study presented a classifier for predicting depression of social network users one year in advance with a 70% classification accuracy compared to a clinical diagnosis. Such results only further confirm the richness of SNSs collected data.


### Beyond Social Networking Services

After gaining the awareness about the level of detail of the information being derived about us, a natural concern may be whether the only feasible way to preserve one's privacy is by removing oneself from the social networking services. But in response, research does not provide comfort to the concerned, on the contrary, studies suggest that some information can be gathered even about people who are not members of the SNSs [22][23].

Two common ways of acquiring knowledge about users outside a given SNS are: exploiting data shared by users about people external to the SNS; and by using data from one SNS to train models that are then used on other platforms to infer undisclosed user information. An example of the first approach is a study showing that "seemingly innocuous combination of knowledge of confirmed contacts between members <&#x2026;> and their email contacts to non-members" helped the authors achieve 85% accuracy for deducing whether two non-members known by the same member are connected [23]. Similarly, an illustration of the second approach is a study [22], where authors "apply the model trained on Facebook Likes to large-scale query logs of a commercial search engine", and conclude that user traits such as age and gender, political and religious views can be accurately inferred based on their search histories.


### Conclusion

Never before were there such vast amounts of personal information of millions of people available for analysis. Exploiting the richness of these SNS data, algorithms are starting to extract not only private information of members, but also information about non-members, and moreover, information that we are maybe not aware of about ourselves. Implications of this trend are far-reaching: on one hand, some insights and predictions, such as the onset of a serious medical condition, have a potential to greatly increase the quality of life of human society; on the other hand, the power of such knowledge can be used for manipulation. Especially, knowing that most of this rich information is concentrated in and available to only a relatively small number of entities, raises a question: what are the chances that incentives of the entities in control of the data are aligned with the well-being of human society? As it usually happens with every powerful technology, that it is being used by people with divergent motivations, probably the same applies here. And one of the first steps in avoiding the undesirable consequences is gaining awareness about the existence and capabilities of this technology, and about our involvement with it.


## References

  - [1] Kwak, Haewoon, et al. "What is Twitter, a social network or a news media?." Proc. of the 19th intl. conf. on World wide web. AcM, 2010.
  - [2] Tufts, Shannon Howle, Willow S. Jacobson, and Mattie Sue Stevens. "Status update: social media and local government human resource practices." Review of Public Personnel Administration 35.2 (2015): 193-207.
  - [3] "Resourcing and talent planning 2017" <https://www.cipd.co.uk/knowledge/strategy/resourcing/surveys> (visited on 2019-05-11)
  - [4] "Survey by The Harris Poll on behalf of CareerBuilder" <http://press.careerbuilder.com/2018-08-09-More-Than-Half-of-Employers-Have-Found-Content-on-Social-Media-That-Caused-Them-NOT-to-Hire-a-Candidate-According-to-Recent-CareerBuilder-Survey> (visited on 2019-05-10)
  - [5] Alexa "The top 500 sites on the web" <https://www.alexa.com/topsites> (visited on 2019-05-11)
  - [6] Song, Indeok, et al. "Internet gratifications and Internet addiction: On the uses and abuses of new media." Cyberpsychology & behavior 7.4 (2004): 384-394.
  - [7] Kuss, Daria J., and Mark D. Griffiths. "Online social networking and addiction—a review of the psychological literature." Intl. journal of environmental research and public health 8.9 (2011): 3528-3552.
  - [8] Smith, Peter K., et al. "An investigation into cyberbullying, its forms, awareness and impact, and the relationship between age and gender in cyberbullying." Research Brief No. RBX03-06. London: DfES (2006).
  - [9] Donegan, Richard. "Bullying and cyberbullying: History, statistics, law, prevention and analysis." The Elon Journal of Undergraduate Research in Communications 3.1 (2012): 33-42.
  - [10] Dougnon, Raïssa Yapan, Philippe Fournier-Viger, and Roger Nkambou. "Inferring user profiles in online social networks using a partial social graph." Canadian Conf. on Artificial Intelligence. Springer, Cham, 2015.
  - [11] Mislove, Alan, et al. "You are who you know: inferring user profiles in online social networks." Proc. of the 3rd ACM intl. conference on Web search and data mining. ACM, 2010.
  - [12] Zhao, Yuchen, et al. "Inferring social roles and statuses in social networks." Proc. of the 19th ACM SIGKDD intl. conf. on Knowledge discovery and data mining. ACM, 2013.
  - [13] Kosinski, Michal, David Stillwell, and Thore Graepel. "Private traits and attributes are predictable from digital records of human behavior." Proc. of the National Academy of Sciences 110.15 (2013): 5802-5805.
  - [14] Goldberg, Lewis R. "An alternative" description of personality": the big-five factor structure." Journal of personality and social psychology 59.6 (1990): 1216.
  - [15] Golbeck, Jennifer, et al. "Predicting personality from twitter." 2011 IEEE 3rd intl. conf. on privacy, security, risk and trust and 2011 IEEE 3rd intl. conf. on social computing. IEEE, 2011.
  - [16] Saha, Koustuv, et al. "Inferring mood instability on social media by leveraging ecological momentary assessments." Proc. of the ACM on Interactive, Mobile, Wearable and Ubiquitous Technologies 1.3 (2017): 95.
  - [17] Zhang, Jiawei, Xiangnan Kong, and S. Yu Philip. "Predicting social links for new users across aligned heterogeneous social networks." 2013 IEEE 13th Intl. Conf. on Data Mining. IEEE, 2013.
  - [18] Yang, Yang, et al. "Predicting links in multi-relational and heterogeneous networks." 2012 IEEE 12th intl. conf. on data mining. IEEE, 2012.
  - [19] Fang, Xiao, et al. "Predicting adoption probabilities in social networks." Information Systems Research 24.1 (2013): 128-145.
  - [20] Yang, Jiang, and Scott Counts. "Predicting the speed, scale, and range of information diffusion in twitter." 4th Intl. AAAI Conf. on Weblogs and Social Media. 2010.
  - [21] De Choudhury, Munmun, et al. "Predicting depression via social media." 7th intl. AAAI conf. on weblogs and social media. 2013.
  - [22] Bi, Bin, et al. "Inferring the demographics of search users: Social data meets search queries." Proc. of the 22nd intl. conf. on World Wide Web. ACM, 2013.
  - [23] Horvát, Emöke-Ágnes, et al. "One plus one makes three (for social networks)." PloS one 7.4 (2012): e34740.
